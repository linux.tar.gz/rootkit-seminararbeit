# Seminar Arbeit zu Rootkits 

## Rahmen 
- 20 Seiten 
- 1 Kurzgefasste Seite 

## Bewertung 
- Inhaltliche Richtigkeit
- Selbststaendigkeit der Erarbeitung

- Vollstaedingkeit der Erarbeitung 
- Gliederung und logische Struktur
- Klarheit von Formulierungen und Argumentation
- Zeitliche Planung und Einhaltung der Termine 
- Zusammenfassung 
- Rueckopplung mit dem Betreuer


## Resourcen 
- The Rootkit Arsenal
- A Guide to Kernel Exploitation
- Rustys Guid to Kernel Hacking: http://es.tldp.org/Presentaciones/200211hispalinux/rusty/seminar.html 
- LKM Guide: http://www.faqs.org/docs/kernel/
- Kernel Newbies: https://kernelnewbies.org/KernelHacking
## Thema 
### ?

## Tools that can detect a Rootkit on a Linux Maschine 
- ChkRootkit 
- Rkhunter
- ISPProtect



