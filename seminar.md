## Vorgehen bei einer wissenschaftlichen Arbeit
1. Zielsetzung: Warum mache ich diese Arbeit und was moechte ich damit erreichen?
2. Forschungsfrage: Ausformulierte Frage die mit der Bearbeitung der vorliegenden Arbeit beantwortet wird.
3. Forschungsmethode: Beschreibung des Vorgehens zur Beantwortung der Frage.
4. Struktur der Arbeit: Aufbauend auf der Forschungsmehtode wird die inhaltliche Struktur festgelegt. (Inhaltsverzeichnis)
5. Expose und Zeitplan: Zu jeden Kapitel eine kurze Beschreibung (1-2 Saetze) und Erstellung eines Zeitplans was wann gemacht wird.
6. Lets goooooooo

## Aufbau einer Arbeit
Titel
Erklaerung
Inhaltsverzeichnis
1. Einleitung, Problemstellung, Motivation  = Einbindung in das wiss. Umfeld
2. Grundlagen, Theorie, Vorarbeiten	    = benoetigte Voraussetyunhen, "Loesungsstrategie"
3. Eigene Arbeiten			    = Neuigkeiten
4. Ergebnisse				    = Schlussfolgerungen
5. Bewertung, Ausblick
Literatur
